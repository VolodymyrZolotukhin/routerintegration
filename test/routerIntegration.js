const Nari = artifacts.require("./nari.sol");
const RouterIntegration = artifacts.require("./routerIntegration.sol");
const Reverter = require("./helpers/reverter");
const truffleAssert = require("truffle-assertions");
const Router =  artifacts.require("./interfaces/IUniswapV2Router02.sol");
const Factory = artifacts.require("./interfaces/IUniswapV2Factory.sol");
const Pair = artifacts.require("./interfaces/IUniswapV2Pair.sol");
const IERC20 = artifacts.require("./node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol");

contract("routerIntegration", async(accounts) => {
    const DEFAULT = accounts[0];
    const ALICE = accounts[1];
    const reverter = new Reverter(web3);
    const ONE_ETH = web3.utils.toBN(web3.utils.toWei("1", "ether"));
    const WETH_ADDRESS = "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2";
    const USDT_ADDRESS = "0xdac17f958d2ee523a2206206994597c13d831ec7";
    const ROUTER_ADDRESS = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";

    const timestamp = () =>{
        return new Date().getTime();
    }

    let nari;
    let routerIntegration;
    let router;
    let factory;
    let usdt;
    let pairETHToken;
    let pairTokenUSDT;

    before("setup", async() => {
        nari = await Nari.new();
        routerIntegration = await RouterIntegration.new(nari.address);
        router = await Router.at(ROUTER_ADDRESS);
        factory = await Factory.at(await router.factory());
        await web3.eth.sendTransaction({from:DEFAULT,to:nari.address,value:ONE_ETH});
        await nari.approve(ROUTER_ADDRESS, await nari.balanceOf(DEFAULT), {from:DEFAULT});
        await router.addLiquidityETH(nari.address, ONE_ETH, ONE_ETH, ONE_ETH,
             ALICE, timestamp()+1000, {from:DEFAULT, value: ONE_ETH});
        pairETHToken = await Pair.at(await factory.getPair(WETH_ADDRESS, nari.address));
        
        await router.swapExactETHForTokens(0, [WETH_ADDRESS,USDT_ADDRESS], DEFAULT, timestamp()+1000,{value:ONE_ETH});

        usdt = await IERC20.at(USDT_ADDRESS);
        await usdt.transfer(routerIntegration.address, await usdt.balanceOf(DEFAULT));
        reverter.snapshot();
    });

    afterEach("revert", reverter.revert);

    describe("addLiq, removeLiq", async() => {
        it("try addLiq on 1 ETH", async() => {
            assert.equal((await nari.balanceOf(DEFAULT)).toString(),"0");
            assert.equal((await usdt.balanceOf(DEFAULT)).toString(),"0");
            
            await routerIntegration.addLiq({from:DEFAULT,value:ONE_ETH});
            pairTokenUSDT = await Pair.at(await factory.getPair(nari.address, USDT_ADDRESS));
            assert.equal((await pairTokenUSDT.balanceOf(routerIntegration.address)) > 0,true);
            //assert.equal(());
            assert.equal((await pairTokenUSDT.getReserves())[1].toString(), "10000");
            let naris = (await pairTokenUSDT.getReserves())[0].toString()

            await routerIntegration.removeLiq();

            assert.equal((await nari.balanceOf(routerIntegration.address)).toString(), "0");
            assert.equal((await pairTokenUSDT.balanceOf(routerIntegration.address)).toString(),"0");
        });
    })
});