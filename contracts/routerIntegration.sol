//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./interface/IUniswapV2Router02.sol";
import "./interface/IUniswapV2Factory.sol";
import "./interface/IUniswapV2Pair.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";


contract routerIntegration is Ownable{
    using SafeERC20 for IERC20;
    IUniswapV2Router02 private constant router = IUniswapV2Router02(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D); 
    address public constant WETH = 0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2;
    address public constant USDT = 0xdAC17F958D2ee523a2206206994597C13D831ec7; 
    address public Nari;

    constructor (address _nari) {
        Nari = _nari;
    }

    receive() external payable {
        addLiq();
    }

    function addLiq() public payable {
        address[] memory addrs = new address[](2);
        addrs[0] = WETH;
        addrs[1] = Nari;
        IERC20 _usdt = IERC20(USDT);
        IERC20 _nari = IERC20(Nari);
        _usdt.safeApprove(address(router), 10000);

        uint256 _amount = router.swapExactETHForTokens{value:msg.value}(0, addrs, address(this), block.timestamp)[1];
        _nari.approve(address(router), _amount);
        router.addLiquidity(Nari, USDT, _amount, 10000, _amount, 10000, address(this), block.timestamp);
        
    }

    function  removeLiq() external onlyOwner {
        IUniswapV2Factory _factory = IUniswapV2Factory(router.factory());
        IUniswapV2Pair _pair = IUniswapV2Pair(_factory.getPair(Nari, USDT));
        _pair.approve(address(router), _pair.balanceOf(address(this)));
        router.removeLiquidity(Nari, USDT, _pair.balanceOf(address(this)), 0, 0, owner(), block.timestamp);
    }
}
