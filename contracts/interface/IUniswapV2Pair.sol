//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.6;

interface IUniswapV2Pair {
    function balanceOf(address owner) external view returns (uint);
    function approve(address spender, uint value) external returns (bool);
}