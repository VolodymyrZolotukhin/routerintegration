//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.6;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract nari is Ownable, ERC20("nari","NR"){
    function mint(address _addr, uint256 _amount) external onlyOwner {
        _mint(_addr, _amount);
    }
    receive() external payable{
        _mint(msg.sender, msg.value);
    }
}